provider "aws" {
  region = "us-west-1"
}

module "my_ec2" {
  source = "../modules/ec2"
  ec2_count = 1
  ami_id = "ami-0ec6517f6edbf8044"
  instance_type = "t2.micro"
}
